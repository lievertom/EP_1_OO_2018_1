#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"
#include <string>

using namespace std;

class Pgm : public Imagem {
private:
	Pgm();
public:
	Pgm(string arquivo);
	~Pgm();
	void decodifica_mensagem();
	
};

#endif	
