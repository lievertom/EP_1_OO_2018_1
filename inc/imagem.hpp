#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>

using namespace std;

class Imagem {

private:
	int inicio;
	int tamanho_msg;
	string chave;
	int posicao;
	string arquivo;
	string identificador;
	char comentario;
	int largura;
	int altura;
	int intensidade;
	Imagem();
protected:
	char * pixel;
public:
	Imagem(string arquivo);
	~Imagem();
	
	int getInicio();
	int getTamanho_msg();
	string getChave();
	char getPixel();

	void decodifica_mensagem();	
	
	
};

#endif	
