#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "pgm.hpp"

using namespace std;

Pgm::Pgm(string arquivo) : Imagem(arquivo) {
}
		
Pgm::~Pgm() {
}

void Pgm::decodifica_mensagem() {
	int posicao = 0;
	int cifra = 0;
	string decifrada = "";

	stringstream(getChave()) >> cifra;

	for (posicao = getInicio(); posicao < (getInicio() + getTamanho_msg()); ++posicao) {
			if (pixel[posicao] < 32) {
				pixel[posicao] = 32;
			}
			if ((pixel[posicao] > 64 && pixel[posicao] < 91) || (pixel[posicao] > 96 && pixel[posicao] < 123)) {
				if ((pixel[posicao] < (97 + cifra) && pixel[posicao] > 96) || (pixel[posicao] < (65 + cifra) && pixel[posicao] > 64)) {
					pixel[posicao] += 26;
				}
				pixel[posicao] -= cifra;
			}		
			decifrada += pixel[posicao];
	}
	cout << "Mensagem: " << decifrada << endl << endl;
	delete[] pixel;	
}
