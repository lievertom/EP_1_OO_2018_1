#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"
#include "pgm.hpp"
#include "menu.hpp"

using namespace std;
	
int main (int argc, char ** argv) {

	cout << endl << "BEM VINDO" << endl << endl;

	string opcao = "1";

	while (opcao == "1") { 
		opcoes();

		cout << "Digite sua opção: ";
		getline (cin, opcao);

		if (opcao == "1") {
			try {		
				opcao1();
			}
			catch (int erro) {
				cout << endl << "Arquivo não encontrado." << endl << endl;
			}
			catch (...) {
				cout << endl << "Arquivo inválido." << endl << endl;
			}
		}
		else if (opcao == "2") {
			opcao2();
		}
		else {
			cout << endl << "Opção inválida." << endl;
			opcao = "1";
		} 
					 
	}
	
	return 0;
}

