#include "menu.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"
#include "pgm.hpp"

using namespace std;


void opcoes() {
	cout << "  _________________________________________" << endl;
	cout << " |					   |" << endl;
	cout << " | Opções:				   |" << endl;
	cout << " | 1. Para decifrar uma mensagem digite: 1 |" << endl;
	cout << " | 2. Para sair digite: 2		   |" << endl;
	cout << " |_________________________________________|" << endl << endl;
}

void opcao1() {
	string arquivo = "";
	string pgm = ".pgm";
	string ppm = ".ppm"; 

	cout << "Digite o caminho da imagem: ";
	getline (cin, arquivo);

	if ((signed char)arquivo.find (ppm) != -1) {
		Imagem imagem1(arquivo);
		imagem1.decodifica_mensagem();
	}
	else if ((signed char)arquivo.find (pgm) != -1) {
		Pgm imagem1(arquivo);
		imagem1.decodifica_mensagem();
	}
	else {
		cout << endl << "Arquivo inválido, só é permitido imagens com extensão '.ppm' ou '.pgm' ." << endl;
	}
}

void opcao2() {
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl;
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl;
	cout << " bye" << endl;
	cout << " bye" << endl;
	cout << " byebyebye  bye    bye   byebye " << endl;
	cout << " byebyebye  bye    bye  bye   bye " << endl;
	cout << " bye	 bye  bye  bye  beybyebye " << endl;
	cout << " bye	 bye  bye  bye  bye      " << endl;
	cout << " byebyebye    byebye    byebye " << endl;
	cout << " byebyebye    byebye     byebye " << endl;
	cout << "		bye " << endl;
	cout << "	       bye " << endl;
	cout << "	      bye        ccccccc " << endl;
	cout << "			cccccccc" << endl;
	cout << "		       ccc	     +++	+++ " << endl;
	cout << "		       ccc	     +++	+++  " << endl;
	cout << "			cccccccc  +++++++++  +++++++++ " << endl;
	cout << "			 ccccccc  +++++++++  +++++++++ " << endl;
	cout << "			             +++   	+++   " << endl;
	cout << "			             +++   	+++  " << endl;
	cout << endl << endl;
}
