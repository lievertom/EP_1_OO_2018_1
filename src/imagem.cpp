#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include<bits/stdc++.h>
#include "imagem.hpp"

using namespace std;

Imagem::Imagem(string arquivo) {
	cout << endl << endl << "Local informado: "<< arquivo << endl;
	ifstream imagem;
	imagem.open(arquivo, ios::binary);
	if (imagem.is_open()) {
		// aloca memória do cabaçalho
		getline (imagem, identificador);
		imagem >> comentario;
		imagem >> inicio;
		imagem >> tamanho_msg;
		imagem >> chave;
		imagem >> largura;
		imagem >> altura;
		imagem >> intensidade;

		// conta o numero de caracters do cabeçalho
		int tamanho_cabecalho = 10;
		int limite;
		for(limite = 1; inicio >= limite; tamanho_cabecalho++) {
    			limite *= 10;
		}
		for(limite = 1; tamanho_msg >= limite; tamanho_cabecalho++) {
    			limite *= 10;
		}
		for(limite = 1; largura >= limite; tamanho_cabecalho++) {
    			limite *= 10;
		}
		for(limite = 1; altura >= limite; tamanho_cabecalho++) {
    			limite *= 10;
		}
		for(limite = 1; intensidade >= limite; tamanho_cabecalho++) {
    			limite *= 10;
		}
		tamanho_cabecalho += chave.size();
    		// pega o comprimento do arquivo
    		imagem.seekg (tamanho_cabecalho, ios::end);
    		posicao = imagem.tellg();
    		imagem.seekg (tamanho_cabecalho, ios::beg);
   
    		// alocando memória
    		pixel = new char [posicao];
   
    		// leitura de um bloco de dados
    		imagem.read (pixel,posicao);
    		imagem.close();
	}
	else {
		throw (10);
	}
}
	
Imagem::~Imagem() {
}

int Imagem::getInicio(){
	return inicio;
}
int Imagem::getTamanho_msg(){
	return tamanho_msg;
}
string Imagem::getChave(){
	return chave;
}
char Imagem::getPixel(){
	return * pixel;
}

void Imagem::decodifica_mensagem() {
	// separa a mensagem codificada
	unsigned char inteiro[3*tamanho_msg];
	int i, j;
	string msg_cod = ""; 

	for (i = 0, posicao = inicio; posicao < (inicio + 3*tamanho_msg); ++posicao, ++i) {
		inteiro[i] = (unsigned char)pixel[posicao] % 10;
	}
	for (i = 0, j = 0; i < 3*tamanho_msg; i += 3, j++) {
		unsigned char rgb = inteiro[i] + inteiro[i+1] + inteiro[i+2] + 64;
		if (rgb == 64) {
			rgb = 32;
		}
		msg_cod += (char)rgb; 
	}
	// gera o alfabeto cifrado
	string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	string alfabeto_cifrado = "";
	bool posicao_letras[26] = {0};
	int tamanho_chave = chave.size();
	
	// insere a palavra-chave no início do alfabeto cifrado
	for (i = 0; i < tamanho_chave; i++) {
		if (chave[i] >= 'A' && chave[i] <= 'Z') {
			if (posicao_letras[chave[i] - 65] == 0) {
				alfabeto_cifrado += chave[i];
				posicao_letras[chave[i] - 65] = 1;
			}
		}
		else if (chave[i] >= 'a' && chave[i] <= 'z') {
			if (posicao_letras[chave[i] - 97] == 0) {
				alfabeto_cifrado += chave[i] - 32;
				posicao_letras[chave[i] - 97] = 1;
			}
		}
	}
	// insere o restante dos caracteres no alfabeto codificado
	for (i = 0; i < 26; i++) {
		if (posicao_letras[i] == 0) {
			posicao_letras[i] = 1;
			alfabeto_cifrado += char(i + 65);
		}
	}
	// mantem a posição de cada letra do alfabeto codificado
	map <char,int> alf_cif;
	for (i = 0; i < 26; i++) {
		alf_cif[alfabeto_cifrado[i]] = i;
	}
	string decifrada = "";
	int tamanho_mensagem = msg_cod.size();
	// decodifica a mensagem
	for (i = 0; i < tamanho_mensagem; i++) {
		if (msg_cod[i] >= 'a' && msg_cod[i] <= 'z') {
			int pos = alf_cif[msg_cod[i] - 32];
			decifrada += alfabeto[pos];
		}
		else if (msg_cod[i] >= 'A' && msg_cod[i] <= 'Z') {
			int pos = alf_cif[msg_cod[i]];
			decifrada += alfabeto[pos];
		}
		else {
			decifrada += msg_cod[i];
		}
	}
	cout << "Mensagem: " << decifrada << endl << endl;
 	delete[] pixel;	
}
   
  

